package cram

import "testing"

var (
	testCard = Card{
		QuestionText: "cow?",
		AnswerText:   "mu.",
	}
)

func TestQuestion(t *testing.T) {
	var subject PresentableCard

	subject = testCard

	if subject.Question() != "cow?" {
		t.Errorf("expected string: cow?, got string: %s", subject.Question())
	}

	if subject.Answer() != "mu." {
		t.Errorf("expected string: mu., got string: %s", subject.Question())
	}
}
