package cram

import "math"

type Score float64

const (
	MinimumScore = 0.0
	MaximumScore = 5.0
	MedianScore  = (MinimumScore + MaximumScore) / 2
)

func (s Score) Round() int64 {
	return int64(math.Round(s.Float64()))
}

func (s Score) Equals(t Score) bool {
	return math.Abs((s - t).Float64()) < 1e-6
}

func (s Score) Float64() float64 {
	return float64(s)
}
