package cram

type Deck struct {
	Cards map[PresentableCard]History
}

func NewDeck() Deck {
	return Deck{
		Cards: make(map[PresentableCard]History),
	}
}

func (d *Deck) Add(card PresentableCard) {
	d.Cards[card] = NewHistory(card)
}

func (d *Deck) Quiz(s Student) {
	for card, history := range d.Cards {
		if history.ReadyForReview() {
			d.Cards[card] = history.UpdateWith(s.Answer(card))
		}
	}
}
