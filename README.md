# cram - a spaced repetition module for Go

Inspiration: [Augmenting Long-term Memory](http://augmentingcognition.com/ltm.html)

Research: [SuperMemo Algorithm](https://help.supermemo.org/wiki/SuperMemo_Algorithm)