package cram

import (
	"time"
)

type Reviewable interface {
	ReadyForReview() bool
}

type RecordableHistory interface {
	UpdateWith(Score) History
}

type History struct {
	PresentableCard

	LastReview time.Time
	NextReview time.Time
	Score      Score
	Interval   int64
	Count      int64
}

func NewHistory(card PresentableCard) History {
	return History{
		PresentableCard: card,
		Score:           MedianScore,
		NextReview:      time.Now(),
	}
}

func (history History) UpdateWith(s Score) History {
	h := History{
		PresentableCard: history.PresentableCard,
		Interval:        interval(history.Count, history.Score),
		Score:           score(history.Score, s),
		Count:           history.Count + 1,
		LastReview:      time.Now(),
	}

	h.NextReview = nextReview(history.LastReview, h.Interval)

	return h
}

func (h History) ReadyForReview() bool {
	return h.NextReview.Before(time.Now())
}

func nextReview(lastReview time.Time, interval int64) time.Time {
	return lastReview.Add(time.Duration(interval) * time.Hour * 24)
}

func score(s, t Score) Score {
	r := s + (0.1 - (MaximumScore-t)*(0.08+(MaximumScore-t)*0.02))

	if r < 1.3 {
		return 1.3
	}

	return r
}

func interval(count int64, score Score) int64 {
	switch count {
	case 0, 1:
		return 1
	case 2:
		return 6
	default:
		return interval(count-1, score) * score.Round()
	}
}
