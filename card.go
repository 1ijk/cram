package cram

type PresentableCard interface {
	Question() string
	Answer() string
}

type Card struct {
	QuestionText string `json:"question_text"`
	AnswerText   string `json:"answer_text"`
}

func (card Card) Question() string {
	return card.QuestionText
}

func (card Card) Answer() string {
	return card.AnswerText
}
