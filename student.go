package cram

type Student interface {
	Answer(PresentableCard) Score
}
