package cram

import "testing"

type gunner struct{}

func (gunner) Answer(PresentableCard) Score {
	return MaximumScore
}

type slacker struct{}

func (slacker) Answer(PresentableCard) Score {
	return MinimumScore
}

func TestAdd(t *testing.T) {
	subject := NewDeck()

	if len(subject.Cards) != 0 {
		t.Errorf("new deck should be empty")
	}

	subject.Add(testCard)

	if len(subject.Cards) != 1 {
		t.Errorf("deck should have one more card after adding")
	}
}

func TestQuiz(t *testing.T) {
	subject := NewDeck()
	subject.Add(testCard)

	subject.Quiz(gunner{})
	if history := subject.Cards[testCard]; !history.Score.Equals(Score(2.6)) {
		t.Errorf("expected recent score of %f, got %f", Score(2.6), history.Score)
	}

	subject.Quiz(slacker{})
	if history := subject.Cards[testCard]; !history.Score.Equals(Score(1.8)) {
		t.Errorf("expected recent score of %f, got %f", Score(1.8), history.Score)
	}
}
